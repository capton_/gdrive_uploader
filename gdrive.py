#!/usr/bin/env python3

from __future__ import print_function

import argparse
import os
import os.path

from googleapiclient.http import MediaFileUpload
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.discovery import Resource


# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly',
          'https://www.googleapis.com/auth/drive.file',
          'https://www.googleapis.com/auth/drive.appdata',
]


def get_service() -> Resource:
    '''
    Return google drive service object
    '''
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.json'):
        creds = Credentials.from_authorized_user_file('token.json', SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'creds.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.json', 'w') as token:
            token.write(creds.to_json())

    return build('drive', 'v3', credentials=creds)


def is_sub_directory(service, folder_id, parent_id) -> bool:
    '''
    Check if given folder is a sub-directory of a given directory

    Parameters:
        -> [service]    ~ google drive service
        -> [folder_id]  ~ id of the folder to check if it is a sub-directory
        -> [parent_id]  ~ if of the parent folder
    '''
    curr_folder_id = folder_id
    while True:
        parents = service.files().get(fileId=curr_folder_id, fields='parents').execute()
        if parents == {}:
            break

        found_id = parents['parents'][0]
        if parent_id != found_id:
            curr_folder_id = found_id
            continue

        return True

    return False


def get_folder_id(service: Resource, name: str, parent_id: str) -> str:
    '''
    Return folder id corresponding to its name inside a given folder id.
    '''

    # print(f'Looking for `{name}` id inside {parent_id} parents folder')

    page_token = None
    while True:
        response = service.files().list(q=f"mimeType='application/vnd.google-apps.folder' and trashed=false and name='{name}'",
                                          spaces='drive',
                                          fields='nextPageToken, files(id, name)',
                                          pageToken=page_token).execute()

        for folder in response.get('files', []):
            curr_folder_id = folder.get('id')

            parent_found = is_sub_directory(service, curr_folder_id, parent_id)
            if (parent_found):
                return curr_folder_id

        page_token = response.get('nextPageToken', None)
        if page_token is None:
            break
    
    return None


def upload_folder(service: Resource, folder_path: str):
    '''
    Upload a given folder to google drive.
    '''
    folder_path = folder_path[:-1] if folder_path[-1] == '/' else folder_path

    if os.path.exists(folder_path) and os.path.isdir(folder_path):
        folder_root_metadata = {
            'name': folder_path.split('/')[-1],
            'mimeType': 'application/vnd.google-apps.folder',
            'parents': ['187zoDwVTZnAKTJsC9c6z2fs5_fUwelDM']
        }
        folder_root = service.files().create(body=folder_root_metadata,
                                   fields='id').execute()
        folder_root_id = folder_root.get('id')

        for root, dirs, files in os.walk(folder_path, topdown=True):
            if '.git' in root or '.ipynb_checkpoints' in root or '.vscode' in root:
                continue

            files = [f for f in files if not f[0] == '.']
            dirs = [d for d in dirs if not d[0] == '.']

            for d in dirs:
                folder_metadata = {
                    'name': d,
                    'mimeType': 'application/vnd.google-apps.folder',
                    'parents': [folder_root_id]
                }
                f = service.files().create(body=folder_metadata,
                                       fields='id').execute()

                folder_root_id = f.get('id') if files == [] else folder_root_id

            splited = root.split('/')
            parent_id = get_folder_id(service, splited[-1], folder_root_id)
            
            for file in files:
                path = root + '/' + file
                file_metadata = {'name': file, 'parents': [parent_id if parent_id else folder_root_id]}

                print(f'Uploading {file_metadata}')

                media = MediaFileUpload(f'{path}', resumable=True)
                service.files().create(
                    body=file_metadata,
                    media_body=media,
                    fields='id'
                ).execute()          

    else:
        print("This folder doesn't exists")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("folder")

    args = parser.parse_args()

    gdrive = get_service()
    upload_folder(gdrive, args.folder)


if __name__ == '__main__':
    main()