# gdrive_uploader

Use this to upload a folder in your google drive.

Steps:
- create a project here: [google console](https://console.cloud.google.com/)
- use OAuth 2.0 authentication
- retrieve your credentials and put them in the root project

Usage:
    ./gdrive folder_name_to_upload
